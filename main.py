import matplotlib.pyplot as plt
import numpy
import pandas as pd
from mlp import MLP
import numpy as np


###################################
# 辅助函数

def load_data():
    train_data = pd.read_csv('./data/mnist_train.csv').values
    test_data = pd.read_csv('./data/mnist_test.csv').values
    train_X = numpy.asarray(train_data[:, 1:])
    train_y = numpy.asarray(train_data[:, 0])
    test_X = numpy.asarray(test_data[:, 1:])
    test_y = numpy.asarray(test_data[:, 0])

    return train_X, train_y, test_X, test_y


def validate(model, test_X, test_y, batch_size):
    acc = 0
    indexs = np.arange(len(test_X))
    steps = len(test_X) // batch_size
    np.random.shuffle(indexs)
    for i in range(steps):
        result = []
        idx = indexs[i * batch_size:(i + 1) * batch_size]
        x = test_X[idx]
        y = test_y[idx]
        outputs = model.forward(x)  # 前向传播
        result.extend(np.argmax(outputs, axis=1))
        acc += sum(int(pred == label) for pred, label in zip(result, y))
    acc /= len(test_X)
    return acc


##############################
# 超参数
num_epochs = 60
batch_size = 30
##############################
# 准备模型和数据
model = MLP(input_size=784, batch_size=batch_size, num_classes=10, lr=0.01, hidden_layer_sizes=(256, 128, 64,),
            activation='relu')
model.parameter()

train_X, train_y, test_X, test_y = load_data()

################################
# 开始训练
train_accuracy = []
test_accuracy = []
traning_loss = []

for epoch in range(num_epochs):
    acc = 0
    test_acc = 0
    loss = 0

    indexs = np.arange(len(train_X))
    steps = len(train_X) // batch_size
    np.random.shuffle(indexs)
    for i in range(steps):
        idx = indexs[i * batch_size:(i + 1) * batch_size]
        x = train_X[idx]
        y = train_y[idx]
        outputs = model.forward(x)  # 前向传播
        loss = np.mean(np.sum(-np.eye(10)[y] * np.log(outputs), axis=-1))
        traning_loss.append(loss)
        model.backward(np.eye(10)[y])

        # if (i + 1) % 100 == 0:
        # print(loss)
        # test_acc = validate(model, test_X, test_y, batch_size)
        # train_acc = validate(model, train_X, train_y, batch_size)
        # test_accuracy.append(test_acc)
        # train_accuracy.append(train_acc)
        #
        # print('Epoch[{}/{}] \t Step[{}/{}] \t TrainAcc = {:.3f} \t TestAcc = {:.3f}'.format(epoch + 1, num_epochs, i + 1,
        #                                                                                       steps, train_acc, test_acc))
    # if epoch == 0:
    #     break
    test_acc = validate(model, test_X, test_y, batch_size)
    train_acc = validate(model, train_X, train_y, batch_size)
    test_accuracy.append(test_acc)
    train_accuracy.append(train_acc)

    # model.lr = model.lr * 0.95  # 学习率衰减

    print('Epoch {}:  Train_Acc={:.5f}  Test_Acc={:.5f}'.format(epoch, train_acc, test_acc))

# 画图，第一个epoch
# plt.figure()
# x = [i for i in range(1, len(test_accuracy) + 1)]
# plt.plot(x, train_accuracy)
# plt.plot(x, test_accuracy)
# plt.legend(['train_accuracy', 'test_accuracy'])
# plt.xlabel("Batch")
# plt.ylabel('Accuracy')
# plt.savefig("784-256-100-sigmoid-60-epochs.png")
# plt.show()

# 画图 所有epoch
plt.figure()
x = [i for i in range(1, len(test_accuracy) + 1)]
plt.plot(x, train_accuracy)
plt.plot(x, test_accuracy)
plt.legend(['train_accuracy', 'test_accuracy'])  # 打出图例
plt.xlabel("Epoch")
plt.ylabel('Accuracy')
# plt.ylim((0, 1))
# plt.title('Sigmoid')
plt.savefig("784-256-128-64-10-relu-30-epochs.png")
plt.show()
