# 基于NumPy搭建MLP完成手写数字分类

## 目录结构
```
|-MLP-By-NumPy
  |-data            存放MNIST数据
  |-convert.py      将MNIST原始数据集转换为CSV格式
  |-mlp.py          全连接神经网络实现
  |-main.py         主函数
```

## 实现思路
我的思路是先实现一个线性层类（LinearLayer），然后根据用户输入的层数，将多个线性层对象连接起来，最后再添加一个SoftMax层就构成了一个完整的MLP。
类的定义与函数的声明如下所示，具体的细节见代码。

```python
class LinearLayer:
    def __init__(self, n_in, n_out, batch_size, activation=None, lr=0.001):
    def forward(self, x):
    def backward(self, dout):
class SoftMax:
    def __init__(self):
    def forward(self, x):
    def backward(self, y):
class MLP:
    def __init__(self, input_size, batch_size, num_classes, lr=0.01, hidden_layer_sizes=(256,), activation='relu'):
    def forward(self, x):
    def backward(self, y):
    def parameter(self):
```
## 快速运行
进入代码文件的主目录，在 Shell 中输入 `python main.py` 即可运行。
