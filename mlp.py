import numpy as np


class LinearLayer:
    def __init__(self, n_in, n_out, batch_size, activation=None, lr=0.01):
        # 权重矩阵，dl要学习的参数
        # np.random.seed(0)
        self.W = np.random.normal(0, 0.01, size=(n_in, n_out))
        # self.W = np.zeros((n_in, n_out))
        # 偏移系数，维度为（输入x的长度，nout？） todo 偏移b的维度？
        self.b = np.zeros((batch_size, n_out))
        # self.b = np.random.normal(0, 0.01, size=(batch_size, n_out))
        self.activation = activation  # 激活函数
        self.lr = lr  # 学习率
        self.batch_size = batch_size  # 批量大小
        self.parameter = {'name': 'Linear', 'size': [n_in, n_out], 'activation': activation}

    def forward(self, x):
        self.x = x
        output = np.dot(x, self.W) + self.b
        if self.activation == 'relu':
            output = np.maximum(0, output)
        if self.activation == 'sigmoid':
            output = 1.0 / (1.0 + np.exp(-output))
        if self.activation == 'tanh':
            output = np.tanh(output)
        self.activated_output = output  # 激活后的输出存起来
        return output

    # dout是指上一层对输出求偏导的结果
    def backward(self, dout):
        # 对激活函数求偏导，因为公式最后有这个项
        if self.activation == 'relu':
            self.activated_output[self.activated_output <= 0] = 0
            self.activated_output[self.activated_output > 0] = 1
            dout = dout * self.activated_output
        if self.activation == 'sigmoid':
            dout = self.activated_output * (1.0 - self.activated_output) * dout
        if self.activation == 'tanh':
            dout = (1 - self.activated_output ** 2) * dout
        dx = np.dot(dout, self.W.T)  # dx是指这一层对输出求偏导的结果

        self.dW = np.dot(self.x.T, dout)  # 损失函数L对第i层参数W的偏导
        self.db = dout  # 损失函数L对第i层偏置b的偏导
        # 更新参数
        self.W = self.W - self.dW * self.lr / self.batch_size
        self.b = self.b - self.db * self.lr / self.batch_size

        return dx


class SoftMax:
    y_hat = []

    def __init__(self):
        super(SoftMax, self).__init__()
        self.parameter = {'name': 'SoftMax'}

    def forward(self, x):
        x_exp = np.exp(x)
        partition = np.sum(x_exp, axis=1, keepdims=True)
        self.y_hat = x_exp / partition
        return self.y_hat

    def backward(self, y):
        # 结果对于上一层的偏导，通过推导化简得出来的，交叉熵
        dout = self.y_hat - y
        return dout


# MLP的实现
class MLP:
    def __init__(self, input_size, batch_size, num_classes, lr=0.01, hidden_layer_sizes=(256,), activation='relu'):
        # 初始化线性层的维度，使用for循环简单实现
        self.layer_list = [[hidden_layer_sizes[i], hidden_layer_sizes[i + 1]]
                           for i in range(len(hidden_layer_sizes) - 1)]
        # 输入层
        self.input_layer = LinearLayer(input_size, hidden_layer_sizes[0], batch_size, activation, lr=lr)
        # 最后分类层
        self.classifier = LinearLayer(hidden_layer_sizes[-1], num_classes, batch_size, 'None', lr=lr)
        # 最后softmax层求概率
        self.softmax = SoftMax()

        self.batch_size = batch_size
        self.lr = lr

        self.layers = [self.input_layer]
        for i in range(len(self.layer_list)):
            self.layers.append(LinearLayer(self.layer_list[i][0], self.layer_list[i][1], batch_size, activation, lr=lr))
        self.layers.append(self.classifier)
        self.layers.append(self.softmax)

    # 向前转播，预测也用这个
    def forward(self, x):
        for layer in self.layers:
            x = layer.forward(x)
        return x

    # 向后传播，更新
    def backward(self, y):
        for layer in reversed(self.layers):
            y = layer.backward(y)

    # 方便打印展示
    def parameter(self):
        for i in range(len(self.layers)):
            print("layer {}: {}".format(i + 1, self.layers[i].parameter))
